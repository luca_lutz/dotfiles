from docx import Document
from datetime import date
import os

def replace_placeholder_with_date(doc, placeholder):
    today_date = date.today().strftime("%d.%m.%Y")
    for paragraph in doc.paragraphs:
        if placeholder in paragraph.text:
            inline = paragraph.runs
            for i in range(len(inline)):
                if placeholder in inline[i].text:
                    text = inline[i].text.replace(placeholder, today_date)
                    inline[i].text = text

# Replace Start Date
# Replace End date
# Ausbildungsjahr -> 
#3. 01.09.21 - 01.09.2022
#3. 01.09.22 - 01.09.2023
#3. 01.09.23 - 01.09.2024
folder = "generated"

def main():
    # Öffne das Word-Dokument
    doc = Document('Vorlage-Tagesbericht.doc')  # Ersetze 'dein_dokument.docx' durch den tatsächlichen Dateinamen

    # Definiere den Platzhalter, den du ersetzen möchtest
    placeholder = "[DATUM]"

    # Ersetze den Platzhalter mit dem aktuellen Datum
    replace_placeholder_with_date(doc, placeholder)

    # Speichere das aktualisierte Dokument
    doc.save(os.path(folder, 'aktualisiertes_dokument.docx'))  # Du kannst den Dateinamen nach Bedarf ändern

if __name__ == "__main__":
    main()
