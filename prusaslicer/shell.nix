let
  lock = builtins.fromJSON (builtins.readFile ./flake.lock);
in
{ pkgs ? import
    (fetchTarball {
      url = "https://github.com/NixOS/nixpkgs/archive/${lock.nodes.nixpkgs.locked.rev}.tar.gz";
      sha256 = lock.nodes.nixpkgs.locked.narHash;
    })
    { }
}:

pkgs.mkShell {
  name = "prusa-slicer";
  packages = with pkgs; [
    prusa-slicer
  ];
  HISTFILE = "${toString ./.}/.history";
}

