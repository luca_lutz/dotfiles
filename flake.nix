{
  description = "lemme smash";

  inputs = {
    nixpkgs.url = "nixpkgs/nixos-23.11";
    nixpkgs-unstable.url = "nixpkgs/nixos-unstable";
    home-manager.url = "github:nix-community/home-manager/release-23.11";
    home-manager.inputs.nixpkgs.follows = "nixpkgs";
    nixos-hardware.url = "github:NixOS/nixos-hardware/master";
  };

  outputs = { nixpkgs, nixpkgs-unstable, nixos-hardware, home-manager, ... }:
    let
      system = "x84_64-linux";
      pkgs = import nixpkgs {
          inherit system;
          config = { allowUnfree = true; };
      };
      lib = nixpkgs.lib;
    in {
      formatter.x86_64-linux = nixpkgs.legacyPackages.x86_64-linux.nixpkgs-fmt;
      nixosConfigurations = {
        desktop = lib.nixosSystem {
          inherit system;
          modules = [
            ./modules
            ./hardware/desktop.nix
            home-manager.nixosModules.home-manager
            {
              home-manager.useGlobalPkgs = true;
              home-manager.useUserPackages = true;
              home-manager.users.luca = import ./home;
            }
            ({ config, pkgs, ... }: 
              let
                overlay-unstable = final: prev: {
                  #unstable = nixpkgs-unstable.legacyPackages.x86_64-linux 
		  unstable = import 
    		  (nixpkgs-unstable)
                  { 
		    system = final.system;
                    config.allowUnfree = true;
                  };
                };
              in
	          {
                nixpkgs.overlays = [ overlay-unstable ]; 
                nixpkgs.config.permittedInsecurePackages = [
                  "electron-12.2.3"
                  "electron-19.1.9"
                ];
		environment.systemPackages = with pkgs; [
	              unstable.hydroxide
                      unstable.citrix_workspace
		      unstable.prusa-slicer
		      unstable.inkscape
	            ];
	          }
	        )
          ];
        };
        qemu = lib.nixosSystem {
          inherit system;
          modules = [
            ./modules
            ./hardware/qemu.nix
            home-manager.nixosModules.home-manager
            {
              home-manager.useGlobalPkgs = true;
              home-manager.useUserPackages = true;
              home-manager.users.luca = import ./home;
            }
          ];
        };
      };
    };
}
