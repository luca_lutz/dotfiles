{ config, pkgs, ... }:
{  
  #pkgs.mkShell {
  #  buildInputs = [
  #    pkgs.platformio
      # optional: needed as a programmer i.e. for esp32
      # pkgs.avrdude
  #  ];
  environment.systemPackages = with pkgs; [
    platformio
    avrdude
    platformio-core
  ];
  services.udev.packages = [ 
    pkgs.platformio-core
    pkgs.openocd
  ];
}
