{ config, pkgs, ... }:
{  
  hardware.rtl-sdr.enable = true;
  boot.kernelParams = [ "modprobe.blacklist=dvb_usb_rtl28xxu" ]; # blacklist this module
  services.udev.packages = with pkgs; [
    rtl-sdr
  ];
}
