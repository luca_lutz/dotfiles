{ config, pkgs, ... }:

{
  # Home Manager needs a bit of information about you and the
  # paths it should manage.
  home.username = "luca";
  home.homeDirectory = "/home/luca";

  # This value determines the Home Manager release that your
  # configuration is compatible with. This helps avoid breakage
  # when a new Home Manager release introduces backwards
  # incompatible changes.
  #
  # You can update Home Manager without changing this value. See
  # the Home Manager release notes for a list of state version
  # changes in each release.
  home.stateVersion = "23.05";

  # Let Home Manager install and manage itself.
  programs.home-manager.enable = true;

  home.packages = with pkgs; [
    killall
    firefox
    thunderbird
    gimp
    spice-vdagent
    neofetch
    alacritty # gpu accelerated terminal
    mako # notification system
    bemenu # launch menu
    openvpn
    element-desktop #-wayland
    thunderbird
    terraform
    nmap
    signal-desktop
    freecad
    drawio
    cargo
    rustc
    spotify
    #inkscape
    fluxcd
    kubeseal
    chromium
    platformio
    usbutils
    #citrix_workspace
    magic-wormhole-rs
    magic-wormhole
    #linphone
    vlc
    ffmpeg
    obs-studio
    #citrix_workspace
    whatsapp-for-linux
    wiimms-iso-tools
    etcher
    prismlauncher
    vscode
    libreoffice
    onlyoffice-bin
    python3
    openjdk17
    #jre17_minimal
    nextcloud-client
    rtl-sdr
    gqrx
  ];

  xdg.enable = true;

  #programs.bash.enable = true;
  programs.fish.enable = true;

  imports = [
    ./kubectl.nix
    ./kdeconnect.nix
    ./dconf.nix
  ];
}
